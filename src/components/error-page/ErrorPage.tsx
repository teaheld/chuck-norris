import { ERROR_404, ERROR_NOT_FOUND } from "../../infrastructure/Utils";

interface ErrorPageProps {
  code?: number;
  message?: string;
}

export default function ErrorPage({
  code = ERROR_404,
  message = ERROR_NOT_FOUND,
}: ErrorPageProps) {
  return (
    <div className="h-70 mt-6 text-white flex flex-column justify-content-center">
      <p className="text-8xl">{code}</p>
      <p className="text-4xl">{message}</p>
    </div>
  );
}
