import { ProgressSpinner } from "primereact/progressspinner";
import "./Loading.css";

interface LoadingProps {
  loading: boolean;
  children: JSX.Element;
}

export default function Loading({ loading, children }: LoadingProps) {
  return loading ? (
    <div className="flex flex-column align-items-center mt-6 h-70">
      <ProgressSpinner strokeWidth="0.5" />
    </div>
  ) : (
    children
  );
}
