import { InputText } from "primereact/inputtext";
import { ChangeEvent } from "react";
import Labels from "../../../infrastructure/Labels";
import "./Input.css";

interface InputProps {
  searchString: string;
  setSearchField: (e: ChangeEvent<HTMLInputElement>) => void;
  search: (e: React.KeyboardEvent<HTMLElement>) => void;
}

export default function Input({
  searchString,
  setSearchField,
  search,
}: InputProps) {
  return (
    <span className="p-input-icon-left w-full">
      <i className="relative pi pi-search text-indigo-900" />
      <InputText
        value={searchString}
        onChange={setSearchField}
        placeholder={Labels.SEARCH}
        className="bg-white text-indigo-900 w-9"
        onKeyDown={search}
      />
    </span>
  );
}
