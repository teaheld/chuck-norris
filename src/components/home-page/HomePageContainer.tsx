import { useCallback, useEffect, useReducer } from "react";
import {
  getFactsBySearchQuery,
  getRandomFact,
} from "../../infrastructure/controllers/FactController";
import basicActions from "../../infrastructure/hooks/basicActions";
import homePageFieldNames from "../../infrastructure/hooks/homePageReducer/homePageFieldNames";
import homePageInitialState from "../../infrastructure/hooks/homePageReducer/homePageInitialState";
import basicReducer from "../../infrastructure/hooks/basicReducer";
import { ENTER } from "../../infrastructure/Utils";
import HomePage from "./HomePage";
import HomePageState from "../../infrastructure/models/HomePageState";
import Loading from "../loading/Loading";
import FactType from "../../infrastructure/models/FactType";
import { useNavigate } from "react-router-dom";
import Labels from "../../infrastructure/Labels";

export default function HomePageContainer() {
  const [state, dispatch] = useReducer(basicReducer, homePageInitialState);

  const navigate = useNavigate();

  const setSearchField = useCallback(
    ({ target: { value } }: { target: { value: string } }) => {
      dispatch({
        type: basicActions.SET_FIELD,
        fieldName: homePageFieldNames.SEARCH_STRING,
        value,
      });
    },
    []
  );

  const navigateToDetailsPage = (id: string) => (_: any) =>
    navigate(`/details/${id}`);

  const search = (e: React.KeyboardEvent<HTMLElement>) => {
    if (e.key === ENTER && homePageFieldNames.SEARCH_STRING in state) {
      const searchString = state.searchString.trim();
      if (
        searchString !== "" &&
        searchString.length >= 3 &&
        searchString.length <= 120
      ) {
        dispatch({
          type: basicActions.SET_FIELD,
          fieldName: homePageFieldNames.LOADING,
          value: true,
        });
        getFactsBySearchQuery(searchString)
          .then(
            ({ data: { result } }: { data: { result: Array<FactType> } }) => {
              dispatch({
                type: basicActions.SET_FIELD_AND_LOADING,
                fieldName: homePageFieldNames.SEARCH_RESULTS,
                value: result.slice(0, 10),
              });
            }
          )
          .catch((error) => {
            console.error(error);
          });
      } else {
        alert(Labels.SEARCH_STRING_VALIDATION);
      }
    }
  };

  useEffect(() => {
    getRandomFact()
      .then(({ data: { value } }: { data: { value: string } }) => {
        dispatch({
          type: basicActions.SET_FIELD_AND_LOADING,
          fieldName: homePageFieldNames.RANDOM_FACT,
          value,
        });
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);

  return (
    <Loading loading={state.loading}>
      <HomePage
        state={state as HomePageState}
        setSearchField={setSearchField}
        search={search}
        navigateToDetailsPage={navigateToDetailsPage}
      />
    </Loading>
  );
}
