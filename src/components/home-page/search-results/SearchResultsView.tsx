import Labels from "../../../infrastructure/Labels";
import FactType from "../../../infrastructure/models/FactType";

interface SearchResultsViewProps {
  searchResults: Array<FactType>;
  navigateToDetailsPage: (id: string) => (_: any) => void;
}

export default function SearchResultsView({
  searchResults,
  navigateToDetailsPage,
}: SearchResultsViewProps) {
  return (
    <div>
      <p className="text-white text-4xl mb-6">{Labels.SEARCH_RESULTS}</p>
      {searchResults.map((searchResult: FactType) => (
        <p
          key={searchResult.id}
          className="text-orange-500 cursor-pointer mx-4"
          onClick={navigateToDetailsPage(searchResult.id)}
        >
          {searchResult.value}
        </p>
      ))}
    </div>
  );
}
