interface RandomFactViewProps {
  randomFact: string;
}

export default function RandomFactView({ randomFact }: RandomFactViewProps) {
  return <p className="mx-8 text-orange-500 text-4xl">{randomFact}</p>;
}
