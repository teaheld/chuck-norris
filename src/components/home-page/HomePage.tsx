import { ChangeEvent } from "react";
import HomePageState from "../../infrastructure/models/HomePageState";
import RandomFactView from "./random-fact/RandomFactView";
import Input from "./input/Input";
import SearchResultsView from "./search-results/SearchResultsView";

interface HomePageProps {
  state: HomePageState;
  setSearchField: (e: ChangeEvent<HTMLInputElement>) => void;
  search: (e: React.KeyboardEvent<HTMLElement>) => void;
  navigateToDetailsPage: (id: string) => (_: any) => void;
}

export default function HomePage({
  state,
  setSearchField,
  search,
  navigateToDetailsPage,
}: HomePageProps) {
  return (
    <div className="py-8">
      <Input
        searchString={state.searchString}
        setSearchField={setSearchField}
        search={search}
      />
      <div className="pt-6">
        {state.searchResults.length > 0 ? (
          <SearchResultsView
            searchResults={state.searchResults}
            navigateToDetailsPage={navigateToDetailsPage}
          />
        ) : (
          <RandomFactView randomFact={state.randomFact} />
        )}
      </div>
    </div>
  );
}
