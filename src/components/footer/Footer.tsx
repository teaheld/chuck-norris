import Images from "../../infrastructure/Images";
import Labels from "../../infrastructure/Labels";

export default function Footer() {
  return (
    <div className="h-7rem bg-primary flex justify-content-center align-items-center absolute bottom-0 w-10">
      <img src={Images.LOGO} alt={Labels.APP_NAME} className="h-6rem" />
    </div>
  );
}
