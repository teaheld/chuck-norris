import { useEffect, useReducer } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { getFactById } from "../../infrastructure/controllers/FactController";
import basicActions from "../../infrastructure/hooks/basicActions";
import basicReducer from "../../infrastructure/hooks/basicReducer";
import detailsPageFieldNames from "../../infrastructure/hooks/detailsPageReducer/detailsPageFieldNames";
import detailsPageInitialState from "../../infrastructure/hooks/detailsPageReducer/detailsPageInitialState";
import DetailsPageState from "../../infrastructure/models/DetailsPageState";
import Loading from "../loading/Loading";
import DetailsPage from "./DetailsPage";

export default function DetailsPageContainer() {
  const { factId } = useParams();
  const [state, dispatch] = useReducer(basicReducer, detailsPageInitialState);

  const navigate = useNavigate();

  useEffect(() => {
    getFactById(factId!)
      .then(({ data: { value } }: { data: { value: string } }) => {
        dispatch({
          type: basicActions.SET_FIELD_AND_LOADING,
          fieldName: detailsPageFieldNames.VALUE,
          value,
        });
      })
      .catch(() => {
        navigate("/error", { replace: true });
      });
  }, [factId, navigate]);

  return (
    <Loading loading={state.loading}>
      <DetailsPage state={state as DetailsPageState} />
    </Loading>
  );
}
