import Images from "../../infrastructure/Images";
import Labels from "../../infrastructure/Labels";
import DetailsPageState from "../../infrastructure/models/DetailsPageState";

interface DetailsPageProps {
  state: DetailsPageState;
}

export default function DetailsPage({ state }: DetailsPageProps) {
  return (
    <div className="flex flex-column align-items-center mt-6 mx-4">
      <img src={Images.LOGO} alt={Labels.APP_NAME} className="w-6 mb-6" />
      <p className="text-white text-xl">{state.value}</p>
    </div>
  );
}
