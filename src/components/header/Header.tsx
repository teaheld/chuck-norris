import Labels from "../../infrastructure/Labels";

export default function Header() {
  return (
    <div className="bg-primary flex justify-content-center align-items-center">
      <p className="text-xl text-white font-bold">{Labels.APP_NAME} </p>
    </div>
  );
}
