import React from "react";
import "./App.css";
import "primereact/resources/themes/lara-dark-blue/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import "/node_modules/primeflex/primeflex.css";
import Header from "./components/header/Header";
import Footer from "./components/footer/Footer";
import AppRouter from "./infrastructure/AppRouter";

function App() {
  return (
    <div className="flex justify-content-center">
      <div className="App w-10 bg-indigo-900 h-screen">
        <Header />
        <AppRouter />
        <Footer />
      </div>
    </div>
  );
}

export default App;
