const Labels = {
  APP_NAME: "CHUCK NORRIS",
  SEARCH: "Search",
  SEARCH_RESULTS: "SEARCH RESULTS",
  SEARCH_STRING_VALIDATION:
    "Search string must be between 3 and 120 characters long",
};

export default Labels;
