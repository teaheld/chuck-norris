import axios from "axios";
import Endpoints from "../Endpoints";

const getRandomFact = () => {
  return axios.get(`${Endpoints.BASE_URL}${Endpoints.RANDOM_SUFIX}`);
};

const getFactById = (factId: string) => {
  return axios.get(`${Endpoints.BASE_URL}/${factId}`);
};

const getFactsBySearchQuery = (searchQuery: string) => {
  return axios.get(
    `${Endpoints.BASE_URL}${Endpoints.SEARCH_QUERY_SUFIX}${searchQuery}`
  );
};

export { getRandomFact, getFactById, getFactsBySearchQuery };
