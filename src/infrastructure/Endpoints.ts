const Endpoints = {
  BASE_URL: "https://api.chucknorris.io/jokes",
  RANDOM_SUFIX: "/random",
  SEARCH_QUERY_SUFIX: "/search?query=",
};

export default Endpoints;
