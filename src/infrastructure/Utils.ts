const ENTER = "Enter";

const ERROR_404 = 404;

const ERROR_NOT_FOUND = "Not found";

export { ENTER, ERROR_404, ERROR_NOT_FOUND };
