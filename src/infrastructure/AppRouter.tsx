import { BrowserRouter, Route, Routes } from "react-router-dom";
import DetailsPageContainer from "../components/details-page/DetailsPageContainer";
import ErrorPage from "../components/error-page/ErrorPage";
import HomePageContainer from "../components/home-page/HomePageContainer";

export default function AppRouter() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<HomePageContainer />} />
        <Route path="/details/:factId" element={<DetailsPageContainer />} />
        <Route path="*" element={<ErrorPage />} />
      </Routes>
    </BrowserRouter>
  );
}
