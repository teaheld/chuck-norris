import DetailsPageState from "../models/DetailsPageState";
import FactType from "../models/FactType";
import HomePageState from "../models/HomePageState";
import basicActions from "./basicActions";

export default function basicReducer(
  state: HomePageState | DetailsPageState,
  action: {
    type: basicActions;
    fieldName?: string;
    value?: string | Array<FactType> | boolean;
  }
) {
  switch (action.type) {
    case basicActions.SET_FIELD:
      return { ...state, [action.fieldName!]: action.value };
    case basicActions.SET_FIELD_AND_LOADING:
      return { ...state, [action.fieldName!]: action.value, loading: false };
    default:
      return state;
  }
}
