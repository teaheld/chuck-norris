const homePageInitialState = {
  loading: true,
  randomFact: "",
  searchString: "",
  searchResults: [],
};

export default homePageInitialState;
