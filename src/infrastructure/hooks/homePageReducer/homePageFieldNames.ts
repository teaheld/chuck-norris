enum homePageFieldNames {
  RANDOM_FACT = "randomFact",
  SEARCH_STRING = "searchString",
  SEARCH_RESULTS = "searchResults",
  LOADING = "loading",
}

export default homePageFieldNames;
