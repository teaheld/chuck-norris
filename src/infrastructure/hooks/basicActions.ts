enum basicActions {
  SET_FIELD = "set_field",
  SET_FIELD_AND_LOADING = "set_field_and_loading",
}

export default basicActions;
