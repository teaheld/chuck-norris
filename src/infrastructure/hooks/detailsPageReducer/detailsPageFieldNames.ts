enum detailsPageFieldNames {
  LOADING = "loading",
  VALUE = "value",
}

export default detailsPageFieldNames;
