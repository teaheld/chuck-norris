interface DetailsPageState {
  value: string;
  loading: boolean;
}

export default DetailsPageState;
