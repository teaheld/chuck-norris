interface FactType {
  id: string;
  value: string;
}

export default FactType;
