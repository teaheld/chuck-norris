import FactType from "./FactType";

interface HomePageState {
  loading: boolean;
  randomFact: string;
  searchString: string;
  searchResults: Array<FactType>;
}

export default HomePageState;
